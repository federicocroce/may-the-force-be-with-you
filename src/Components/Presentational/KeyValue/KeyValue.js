import React from 'react';
// import Parser from 'html-react-parser';

import './_keyValue.scss'

const KeyValue = (props) => {

    return (
        <div className='key-value'>
            {props.data.map((item, index) => {
                item.value = item.value == undefined ? '' : item.value
                return (
                    <div className='container table' key={index}>
                        <label>{item.key}</label>
                        <span>{item.value}</span>
                    </div>
                )
            })
            }
        </div>
    )
};

export default KeyValue;

import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import "./_openingCrawl.scss";

class OpeningCrawl extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listFilms: []
    };
  }

  componentWillMount() {
    // fetch("https://swapi.co/api/films")
    //   .then(response => response.json())
    //   .then(films => {
    //     console.log(films);
    //     this.setState({ listFilms: films.results });
    //   });
  }

  render() {
    const { props } = this;
    // const data = [
    //   {
    //     key: "Titulo",
    //     value: "Valor"
    //   }
    // ];
    return (
      <section className="opening-crawl">
        <div className="content">
          <p>{props.content}</p>
        </div>
      </section>
    );
  }
}

export default withRouter(OpeningCrawl);

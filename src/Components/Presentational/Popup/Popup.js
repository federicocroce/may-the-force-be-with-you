import React from 'react';
import './_popup.scss'

class Popup extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { props } = this;
    const { close } = props;
    return (
        <article className="popup">
          <div className="popup-content">
            <header className="header--dynamic-content">
              {props.title ? <h1>{props.title}</h1> : null}
              <span className="icon-close close-popup" onClick={() => close()}>CERRAR</span>
            </header>

            <div className="children-dynamic-content">
              {props.children}
            </div>

          </div>
        </article>
    );
  }
}

export default Popup;
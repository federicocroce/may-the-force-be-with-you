import React from 'react';
import './_card.scss'

const Card = props => {

    return (
        <div 
            onClick={props.onClick ? ()=>props.onClick() : null}
            onBlur={props.onBlur ? ()=>props.onBlur() : null}
            tabIndex="0"
            className={`card ${props.className ? props.className : ''} `}
            >

            {props.children}
        </div>
    );
}

export default Card;


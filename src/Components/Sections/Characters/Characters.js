import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import Card from "../../Presentational/Card/Card";
import KeyValue from "../../Presentational/KeyValue/KeyValue";
import Popup from "../../Presentational/Popup/Popup";
import OpeningCrawl from "../../Presentational/OpeningCrawl/OpeningCrawl";
import Spinner from "../../Presentational/Spinner/Spinner";

import "./characters.scss";

class Characters extends Component {
  constructor(props) {
    super(props);

    this.state = {
      characterList: [],
      episode: "",
      showPopup: false,
      isLoading: false
    };
  }

  componentWillMount() {

    if (!this.props.location.data) {
      this.props.history.push("/");
      return;
    }

    this.setState({
      episode: this.props.location.data.episode,
      isLoading: true
    });

    const characterList =
      this.props.location.data.characters.length >= 10
        ? this.props.location.data.characters.splice(
            this.props.location.data.characters.length - 10
          )
        : this.props.location.data.characters;

    characterList.map(url => {
      fetch(url)
        .then(response => response.json())
        .then(character => {
          console.log(character);
          this.setState(state => {
            return {
              isLoading: false,
              characterList: [
                ...state.characterList,
                (character = {
                  name: character.name,
                  data: [
                    {
                      key: "Color de Ojos",
                      value: character.eye_color
                    },
                    {
                      key: "Género",
                      value: character.gender
                    }
                  ],
                  films: {
                    key: "Films",
                    value: this.getDataFilms(
                      this.props.location.data.films,
                      character.films
                    )
                  }
                })
              ]
            };
          });
        });
    });
  }

  getDataFilms = (films, characterFilm) =>
    characterFilm.map(
      urlFilmCharater => films.filter(film => film.url == urlFilmCharater)[0]
    );

  openModal = ({ opening_crawl }) => {
    this.setState({
      showPopup: true,
      opening_crawl
    });
  };

  render() {
    const { state } = this;
    return (
      <section className="characters">
        <h2>PERSONAJES DEL EPISODIO {this.state.episode}</h2>
        {!state.isLoading ? (
          <div className="list-container">
            {state.characterList.map((character, index) => (
              <Card key={index}>
                <h3>{character.name}</h3>
                <KeyValue data={character.data}></KeyValue>
                <div>
                  <span>{character.films.key}</span>
                  <ul>
                    {character.films.value.map(film => (
                      <li onClick={() => this.openModal(film)} className="link">
                        {film.title}
                      </li>
                    ))}
                  </ul>
                </div>

                {state.showPopup && (
                  <Popup
                    close={() => {
                      this.setState({ showPopup: false });
                    }}
                  >
                    <OpeningCrawl content={state.opening_crawl}></OpeningCrawl>
                  </Popup>
                )}
              </Card>
            ))}
          </div>
        ) : (
          <Spinner />
        )}
      </section>
    );
  }
}

export default withRouter(Characters);

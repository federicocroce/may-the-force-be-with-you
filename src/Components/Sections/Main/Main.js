import React, { Fragment } from "react";
import { NavLink, Route, BrowserRouter } from "react-router-dom";

import "../../../App.scss";
import "./_main.scss";

import Home from "../Home/Home";
import Characters from "../Characters/Characters";

const Main = () => (
  <div className="main">
    <BrowserRouter>
      <div className="header">
        <h1>STAR WARS</h1>
        <NavLink className="link" to={{ pathname: `/` }}>
          HOME
        </NavLink>
      </div>
      <Route exact path="/" component={Home}></Route>
      <Route path={`/characters`} component={Characters}></Route>
    </BrowserRouter>
  </div>
);

export default Main;

import React, { Component } from "react";
import { withRouter, NavLink } from "react-router-dom";

import Card from "../../Presentational/Card/Card";
import OpeningCrawl from "../../Presentational/OpeningCrawl/OpeningCrawl";
import Spinner from "../../Presentational/Spinner/Spinner";

import "./_home.scss";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listFilms: [],
      dataToCharaters: [],
      filmsSelected: {},
      isLoading: false
    };
  }

  componentWillMount() {
    this.setState({isLoading: true})
    fetch("https://swapi.co/api/films")
      .then(response => response.json())
      .then(films => {
        console.log(films);
        this.setState({ listFilms: films.results });
        this.formatDataToCharaters(films.results);
        this.setState({isLoading: false})
      });
  }

  formatDataToCharaters = films => {
    const dataToCharaters = films.map(({ title, opening_crawl, url }) => {
      return {
        title,
        opening_crawl,
        url
      };
    });

    this.setState({ dataToCharaters });
  };

  selectFilm = index => {
    console.log(index);
    console.log(this.state.filmsSelected);
    const value = {
      [index]: true
    };
    this.setState({ filmsSelected: value });
  };

  render() {
    const { state } = this;

    return (
      <section className="home">
        <h2>FILMS</h2>
        {!state.isLoading ? <div className="list-container">
          {state.listFilms.map((film, index) => (
            <Card key={index}>
              <h3 onClick={() => this.selectFilm(index)}>
                EPISODIO {film.episode_id}
              </h3>
              <p>{film.title}</p>
              <p>
                <span className="director">DIRECTOR:</span> {film.director}
              </p>
              <NavLink
                className="link"
                to={{
                  pathname: `/characters`,
                  data: {
                    characters: film.characters,
                    episode: film.episode_id,
                    films: state.dataToCharaters
                  }
                }}
              >
                PERSONAJES
              </NavLink>
              {this.state.filmsSelected[index] && (
                <OpeningCrawl content={film.opening_crawl}></OpeningCrawl>
              )}
            </Card>
          ))}
        </div>
        : <Spinner/>}
      </section>
    );
  }
}

export default withRouter(Home);
